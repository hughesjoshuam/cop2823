﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IntroToCss.aspx.cs" Inherits="RentMyWrox.IntroToCss" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>COP2823-Designing Your Web Pages</title>
    <link href="Content/IntroToCss.css" type="text/css" rel="stylesheet" />
    <style type="text/css">
        #form1 {
            margin-left: 40px;
        }
    </style>
</head>
<body>
    <header>
        <hgroup style="margin-left: 40px">
            <h1><span class="introduction">Chapter 3</span> - Designing Your Web Pages</h1>
            <h2 style="margin-left: 40px">COP2823</h2>
        </hgroup>
    </header>
    <form id="form1" runat="server">
        <div class="special">
            <p style="margin-left: 40px">I am Special! My Text is Special. I wrote some more special text...</p>
        </div>
    </form>
</body>
</html>
