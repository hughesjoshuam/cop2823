﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentMyWrox.Models;

namespace RentMyWrox.Controllers
{
    public class DemoModelController : Controller
    {
        /*
        //GET: DemoModel
        public ActionResult Index()
        {
            List<DemoModel> list = new List<DemoModel>();
            for (int i = 0; i <= 5; i++)
            {
                list.Add(new DemoModel
                {
                    Property1 = i.ToString(),
                    Property2 = string.Format("Property2 {0}", i),
                    Property3 = string.Format("Property3 {0}", i)
                });
            }
            return View("DemoModelList", list);
        }
        */
        // GET: DemoMode
        public ActionResult Index()
        {
            return View("Index");
        }
        // GET: DemoModel/Details/5
        public ActionResult Details(int id)
        {
            DemoModel dm = new DemoModel
            {
                Property1 = id.ToString(),
                Property2 = string.Format("Property2 {0}", id),
                Property3 = string.Format("Property3 {0}", id)
            };
            return View("Details", dm);
        }
        
        // GET: DemoModel/Create
        public ActionResult Create(int id)
        {
            DemoModel dm = new DemoModel
            {
                Property1 = id.ToString(),
                Property2 = string.Format("Property2 {0}", id),
                Property3 = string.Format("Property3 {0}", id)
            };
            return View("Create", dm); // this view will be the form that needs to be filled out
        }
        // POST: DemoMode/Create
        [HttpPost]
        public ActionResult Create(int id, DemoModel model)
        {
            // Do some work to create
            return View("Create"); // View to confirm that a new item was created
        }
        // GET: DemoModel/Edit/5
        public ActionResult Edit(int id)
        {
            DemoModel dm = new DemoModel
            {
                Property1 = id.ToString(),
                Property2 = string.Format("Property2 {0}", id),
                Property3 = string.Format("Property3 {0}", id)
            };
            return View("Edit", dm);
        }
        // POST: DemoModel/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, DemoModel model)
        {
            // do some work here to save edits
            return View("Edit"); // view to confirm that the item was edited
        }
        public ActionResult Delete(int id)
        {
            DemoModel dm = new DemoModel
        {
            Property1 = id.ToString(),
            Property2 = string.Format("Property2 {0}", id),
            Property3 = string.Format("Property3 {0}", id)
        };
            return View("Delete", dm);
        }
    }
}