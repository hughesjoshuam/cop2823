﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageItem.aspx.cs" Inherits="RentMyWrox.Admin.ManageItem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <style>
        .dataentry input{
            width: 250px;
            margin-left: 20px;
            margin-top: 15px;
        }

        .dataentry textarea{
            width: 250px;
            margin-left: 20px;
            margin-top:15px;
        }

        .dataentry label{
            width: 75px;
            margin-left: 20px;
            margin-top: 15px;
        }
        #fuPicture {
            margin-top: -20px;
            margin-left: 120px;
        }
    </style>
    <div style="margin-top:100px;" >
        <div class="dataentry">
            <asp:Label ID="Label1" runat="server" Text="Name" AssociatedControlID="tbName" />
            <asp:TextBox ID="tbName" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label2" runat="server" Text="Description" AssociatedControlID="tbDescription" />
            <asp:TextBox ID="tbDescription" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label6" runat="server" Text="Cost" AssociatedControlID="tbCost" />
            <asp:TextBox ID="tbCost" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label3" runat="server" Text="Item Number" AssociatedControlID="tbItemNumber" />
            <asp:TextBox ID="tbItemNumber" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label4" runat="server" Text="Picture" AssociatedControlID="fuPicture" />
            <asp:FileUpload ID="fuPicture" ClientIDMode="Static" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label5" runat="server" Text="Acquired Date" AssociatedControlID="tbAcquiredDate" />
            <asp:TextBox ID="tbAcquiredDate" runat="server" />
        </div>
        <asp:Button runat="server" Text="Save Item" OnClick="SaveItem_Clicked"/>
    </div>
</asp:Content>
