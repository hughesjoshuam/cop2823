﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebForms.Master" AutoEventWireup="true" CodeBehind="ManageItem.aspx.cs" Inherits="RentMyWrox.Admin.ManageItem" 
    MetaTagDescription="Manage the items that are available to be checked out from the library"
    MetaTagKeywords="Tools, Lending Library, Manage items, actual useful keywords here" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <!-- 
       The book wanted the user control here. I didn't like the way it looked:
        <RMW:NotificationsControl runat="server" />
       -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <RMW:NotificationsControl runat="server" />
        <div class="dataentry">
            <asp:Label ID="Label1" runat="server" Text="Name" AssociatedControlID="tbName" />
            <asp:TextBox ID="tbName" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label2" runat="server" Text="Description" AssociatedControlID="tbDescription" />
            <asp:TextBox ID="tbDescription" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label6" runat="server" Text="Cost" AssociatedControlID="tbCost" />
            <asp:TextBox ID="tbCost" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label3" runat="server" Text="Item Number" AssociatedControlID="tbItemNumber" />
            <asp:TextBox ID="tbItemNumber" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label4" runat="server" Text="Picture" AssociatedControlID="fuPicture" />
            <asp:FileUpload ID="fuPicture" ClientIDMode="Static" runat="server" />
        </div>
        <div class="dataentry">
            <asp:Label ID="Label5" runat="server" Text="Acquired Date" AssociatedControlID="tbAcquiredDate" />
            <asp:TextBox ID="tbAcquiredDate" runat="server" />
        </div>
        <asp:Button runat="server" Text="Save Item" OnClick="SaveItem_Clicked"/>
    </div>
</asp:Content>
