﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ACME_Medical.Startup))]
namespace ACME_Medical
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
