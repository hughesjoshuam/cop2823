namespace GuitarShop.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCommit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        categoryID = c.Int(nullable: false, identity: true),
                        categoryName = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.categoryID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        productID = c.Int(nullable: false, identity: true),
                        categoryID = c.Int(nullable: false),
                        productCode = c.String(maxLength: 10),
                        productName = c.String(maxLength: 255),
                        listPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.productID)
                .ForeignKey("dbo.Categories", t => t.categoryID, cascadeDelete: true)
                .Index(t => t.categoryID);
            
            CreateTable(
                "dbo.Order_Product_Details",
                c => new
                    {
                        orderID = c.Int(nullable: false),
                        productID = c.Int(nullable: false),
                        quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.orderID, t.productID })
                .ForeignKey("dbo.Orders", t => t.orderID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.productID, cascadeDelete: true)
                .Index(t => t.orderID)
                .Index(t => t.productID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        orderID = c.Int(nullable: false, identity: true),
                        customerID = c.Int(nullable: false),
                        orderDate = c.DateTime(nullable: false),
                        orderDescription = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.orderID)
                .ForeignKey("dbo.Customers", t => t.customerID, cascadeDelete: true)
                .Index(t => t.customerID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        customerID = c.Int(nullable: false, identity: true),
                        customerName = c.String(maxLength: 45),
                        customerStreetAddress = c.String(maxLength: 255),
                        customerCity = c.String(maxLength: 45),
                        customerState = c.String(maxLength: 2),
                        customerPostalCode = c.String(maxLength: 8),
                    })
                .PrimaryKey(t => t.customerID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Order_Product_Details", "productID", "dbo.Products");
            DropForeignKey("dbo.Order_Product_Details", "orderID", "dbo.Orders");
            DropForeignKey("dbo.Orders", "customerID", "dbo.Customers");
            DropForeignKey("dbo.Products", "categoryID", "dbo.Categories");
            DropIndex("dbo.Orders", new[] { "customerID" });
            DropIndex("dbo.Order_Product_Details", new[] { "productID" });
            DropIndex("dbo.Order_Product_Details", new[] { "orderID" });
            DropIndex("dbo.Products", new[] { "categoryID" });
            DropTable("dbo.Customers");
            DropTable("dbo.Orders");
            DropTable("dbo.Order_Product_Details");
            DropTable("dbo.Products");
            DropTable("dbo.Categories");
        }
    }
}
