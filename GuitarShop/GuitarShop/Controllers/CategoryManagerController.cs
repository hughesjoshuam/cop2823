﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GuitarShop.Models;

namespace GuitarShop.Controllers
{
    public class CategoryManagerController : Controller
    {
        // GET: CategoryManager
        public ActionResult Index()
        {
            using(GuitarShopContext context = new GuitarShopContext())
            {
                var categories = context.Categories.ToArray();
                return View(categories);
            }
        }

        // GET: CategoryManager/Create
        public ActionResult AddCategory()
        {
            return View();
        }

        // POST: CategoryManager/Create
        [HttpPost]
        public ActionResult AddCategory(Categories obj)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    context.Categories.Add(obj);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View("Index");
            }
        }

        // GET: CategoryManager/Edit/5
        public ActionResult EditCategory(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var category = context.Categories.FirstOrDefault(x => x.categoryID == id);
                return View(category);
            }
        }

        // POST: CategoryManager/Edit/5
        [HttpPost]
        public ActionResult EditCategory(int id, FormCollection collection)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    var category = context.Categories.FirstOrDefault(x => x.categoryID == id);
                    TryUpdateModel(category);
                    context.SaveChanges();
                    return RedirectToAction("Index");

                }
            }
            catch
            {
                return View("Index");
            }
        }

        // GET: CategoryManager/Delete/5
        public ActionResult DeleteCategory(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var category = context.Categories.FirstOrDefault(x => x.categoryID == id);
                return View(category);
            }
        }

        // POST: CategoryManager/Delete/5
        [HttpPost]
        public ActionResult DeleteCategory(int id, FormCollection collection)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    var category = context.Categories.FirstOrDefault(x => x.categoryID == id);
                    context.Categories.Remove(category);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View("Index");
            }
        }
    }
}
