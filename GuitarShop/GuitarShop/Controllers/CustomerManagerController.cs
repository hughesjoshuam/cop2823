﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GuitarShop.Models;

namespace GuitarShop.Controllers
{
    public class CustomerManagerController : Controller
    {
        // GET: CustomerManager
        public ActionResult Index()
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var customers = context.Customers.ToArray();
                return View(customers);
            }
        }

        // GET: CustomerManager/Create
        public ActionResult AddCustomer()
        {
            return View();
        }

        // POST: CustomerManager/Create
        [HttpPost]
        public ActionResult AddCustomer(Customers obj)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    context.Customers.Add(obj);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View("Index");
            }
        }

        // GET: CustomerManager/Edit/5
        [HttpGet]
        public ActionResult EditCustomer(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var customer = context.Customers.FirstOrDefault(x => x.customerID == id);
                return View(customer);
            }
        }

        // POST: CustomerManager/Edit/5
        [HttpPost]
        public ActionResult EditCustomer(int id, FormCollection collection)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    var customer = context.Customers.FirstOrDefault(x => x.customerID == id);
                    TryUpdateModel(customer);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View("Index");
            }
        }

        // GET: CustomerManager/Delete/5
        [HttpGet]
        public ActionResult DeleteCustomer(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var customer = context.Customers.FirstOrDefault(x => x.customerID == id);
                return View(customer);
            }
        }

        // POST: CustomerManager/Delete/5
        [HttpPost]
        public ActionResult DeleteCustomer(int id, FormCollection collection)
        {
            try
            {
                using(GuitarShopContext context = new GuitarShopContext())
                {
                    var customer = context.Customers.FirstOrDefault(x => x.customerID == id);
                    context.Customers.Remove(customer);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View("Index");
            }
        }
    }
}
