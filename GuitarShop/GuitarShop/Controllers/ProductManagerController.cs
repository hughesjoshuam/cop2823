﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GuitarShop.Models;

namespace GuitarShop.Controllers
{
    public class ProductManagerController : Controller
    {
        // GET: ProductManager
        [HttpGet]
        public ActionResult Index(int? id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                if (id != null)
                {
                    var products = context.Products.Include("Categories").Where(x => x.categoryID == id).ToArray();
                    return View(products);
                }
                else
                {
                    var products = context.Products.Include("Categories").ToArray();
                    return View(products);
                }

            }
        }

        // GET: AddProduct
        [HttpGet]
        public ActionResult AddProduct()
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var model = new Products
                {
                    categoryNameSelectList = context.Categories.Select(c => new SelectListItem
                    {
                        Value = c.categoryID.ToString(),
                        Text = c.categoryName
                    }).ToList()
                };
                return View(model);
            }            
        }

        [HttpPost]
        public ActionResult AddProduct(Products obj)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    context.Products.Add(obj);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View("Index");
            }            
        }

        // GET: EditProduct
        [HttpGet]
        public ActionResult EditProduct(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var product = context.Products.Include("Categories").FirstOrDefault(x => x.productID == id);
                product.categoryNameSelectList = context.Categories.Select(c => new SelectListItem
                {
                    Value = c.categoryID.ToString(),
                    Text = c.categoryName
                }).ToList();
                return View(product);
            }
        }
        [HttpPost]
        public ActionResult EditProduct(int id, FormCollection collection)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    var product = context.Products.FirstOrDefault(x => x.productID == id);
                    TryUpdateModel(product);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View("Index");
            }
        }

        // GET: DeleteProduct
        [HttpGet]
        public ActionResult DeleteProduct(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var product = context.Products.Include("Categories").FirstOrDefault(x => x.productID == id);
                return View(product);
            }
        }

        // POST: DeleteProduct
        [HttpPost]
        public ActionResult DeleteProduct(int id, FormCollection collection)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    var product = context.Products.FirstOrDefault(x => x.productID == id);
                    context.Products.Remove(product);
                    context.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return View("Index");
            }
        }
    }
}