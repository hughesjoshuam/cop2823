﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GuitarShop.Models;
using System.Web.UI.WebControls;

namespace GuitarShop.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [HttpGet]
        public ActionResult Index(int? id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                if (id != null)
                {
                    var products = context.Products.Include("Categories").Where(x => x.categoryID == id).ToArray();
                    return View(products);
                } else
                {
                    var products = context.Products.Include("Categories").ToArray();
                    return View(products);
                }
            }
        }
        
        // GET: ProductDetails
        public ActionResult ProductDetails(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var products = context.Products.Include("Categories").FirstOrDefault(p => p.productID == id);
                return View(products);
            }            
        }
    }
}