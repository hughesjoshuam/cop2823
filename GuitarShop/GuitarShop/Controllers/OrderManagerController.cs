﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GuitarShop.Models;

namespace GuitarShop.Controllers
{
    public class OrderManagerController : Controller
    {
        // GET: OrderManager
        public ActionResult Index()
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var orders = context.Orders.Include("Customers").ToArray();
                return View(orders);
            };
        }

        // GET: OrderManager/Details/5
        public ActionResult OrderDetails(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext()) {

                var order = context.Orders.Include("Customers").FirstOrDefault(o => o.orderID == id);
                return View(order);

            }
        }

        // GET: OrderManager/Create
        public ActionResult AddOrder()
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    var orders = new Orders
                    {
                        customerSelectListItem = context.Customers.Select(cust => new SelectListItem
                        {
                            Value = cust.customerID.ToString(),
                            Text = cust.customerName
                        }).ToList(),
                        orderDate = DateTime.Now
                    };

                    return View(orders);
                }
            } catch
            {
                return RedirectToAction("AddOrder");
            }
        }

        // POST: OrderManager/Create
        [HttpPost]
        public ActionResult AddOrder(Orders obj, FormCollection collection)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                // Add Order
                context.Orders.Add(obj);
                context.SaveChanges();
                //Add Order Details
                foreach (var key in collection.AllKeys)
                {
                    if(key.ToString().Contains("productQty"))
                    {
                        var value = collection[key.ToString()];
                        int quantity;
                        bool parseResult = int.TryParse(value.ToString(), out quantity);
                        if(parseResult && quantity > 0)
                        {
                            int productID;
                            parseResult = int.TryParse(key.Substring(10), out productID);
                            Order_Product_Details order_product_details = new Order_Product_Details
                            {
                                orderID = obj.orderID,
                                productID = productID,
                                quantity = quantity
                            };
                            context.Order_Product_Details.Add(order_product_details);
                            context.SaveChanges();
                        }
                    }
                }

                // Redirect to add products to Order
                return RedirectToAction("Index");
            }
        }
        
        // GET: OrderManager/Edit/5
        public ActionResult EditOrder(int id)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    var orders = context.Orders.Include("Customers").FirstOrDefault(o => o.orderID == id);
                    orders.customerSelectListItem = context.Customers.Select(cust => new SelectListItem
                    {
                        Value = cust.customerID.ToString(),
                        Text = cust.customerName
                    }).ToList();

                    return View(orders);
                }
            }
            catch
            {
                return View();
            }
        }

        // POST: OrderManager/Edit/5
        [HttpPost]
        public ActionResult EditOrder(int id, FormCollection collection)
        {
            try
            {
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    // Edit Order
                    var orders = context.Orders.FirstOrDefault(o => o.orderID == id);
                    TryUpdateModel(orders);
                    context.SaveChanges();

                    // Edit Order Product Details
                    string[] aryProductIDS = collection["productDetail.productID"].Split(',');
                    string[] quantities = collection["productDetail.quantity"].Split(',');

                    // Loop through collections
                    for (int index = 0; index < aryProductIDS.Count(); index++)
                    {
                        int productId = int.Parse(aryProductIDS[index]);
                        int quantity = int.Parse(quantities[index]);
                        var order_product_details = context.Order_Product_Details.FirstOrDefault(opd => opd.orderID == orders.orderID && opd.productID == productId);
                        order_product_details.quantity = quantity;
                        TryUpdateModel(order_product_details);
                        context.SaveChanges();
                    }

                    // Redirect to add products to Order
                    return RedirectToAction("Index");
                }               
            }
            catch
            {
                return View("Index");
            }
        }

        // GET: OrderManager/Delete/5
        public ActionResult DeleteOrder(int id)
        {
            using (GuitarShopContext context = new GuitarShopContext())
            {
                var delete = context.Orders.Include("Customers").FirstOrDefault(o => o.orderID == id);
                return View(delete);
            }
        }

        // POST: OrderManager/Delete/5
        [HttpPost]
        public ActionResult DeleteOrder(int id, FormCollection collection)
        {
            try {             
                using (GuitarShopContext context = new GuitarShopContext())
                {
                    //Remove order_product_details
                    var order_products_details = context.Order_Product_Details.Where(o_p_d => o_p_d.orderID == id).ToArray();
                    foreach (var item in order_products_details)
                    {
                        var order_products_detail = context.Order_Product_Details.FirstOrDefault(opd => opd.orderID == item.orderID && opd.productID == item.productID);
                        context.Order_Product_Details.Remove(order_products_detail);
                    }

                    //Remove order
                    var order = context.Orders.FirstOrDefault(o => o.orderID == id);
                    context.Orders.Remove(order);

                    context.SaveChanges();

                    return RedirectToAction("Index");

                }
            }
            catch
            {
                return View();
            }
        }
    }
}
