﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace GuitarShop.Models
{
    public class Orders
    {
        [Key] public int orderID { get; set; }
        public int customerID { get; set; }
        public DateTime orderDate { get; set; }
        [MaxLength(255)]
        public string orderDescription { get; set; }
        public virtual Customers Customers { get; set; }
        public virtual ICollection<Order_Product_Details> Order_Product_Details { get;set; }
        public IEnumerable<SelectListItem> customerSelectListItem { get; set; }

    }
}