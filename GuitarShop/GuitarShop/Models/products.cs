﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace GuitarShop.Models
{
    public class Products
    {        
        [Key] public int productID { get; set; }
        public int categoryID { get; set; }
        [MaxLength(10)]
        public string productCode { get; set; }
        [MaxLength(255)]
        public string productName { get; set; }
        public decimal listPrice { get; set; }
        public virtual Categories Categories { get; set; }
        public virtual ICollection<Order_Product_Details> Order_Product_Details { get; set; }
        public IEnumerable<SelectListItem> categoryNameSelectList { get; set; }
    }
}