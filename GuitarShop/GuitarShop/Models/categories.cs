﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace GuitarShop.Models
{
    public class Categories
    {
        [Key] public int categoryID { get; set; }
        [MaxLength(255)]
        public string categoryName { get; set; }
        public virtual ICollection<Products> Products { get; set; }
    }
}