﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using GuitarShop.Models;

namespace GuitarShop.Models
{
    public class GuitarShopContext : DbContext
    {
        public GuitarShopContext() : base("name=GuitarShopContext") { }
        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Order_Product_Details> Order_Product_Details { get; set; }
    }
}