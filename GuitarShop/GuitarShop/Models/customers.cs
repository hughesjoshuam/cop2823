﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace GuitarShop.Models
{
    public class Customers
    {
        [Key] public int customerID { get; set; }
        [MaxLength(45)]
        public string customerName { get; set; }
        [MaxLength(255)]
        public string customerStreetAddress { get; set; }
        [MaxLength(45)]
        public string customerCity { get; set; }
        [MaxLength(2)]
        public string customerState { get; set; }
        [MaxLength(8)]
        public string customerPostalCode { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}