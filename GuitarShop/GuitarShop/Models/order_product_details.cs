﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GuitarShop.Models
{
    public class Order_Product_Details
    {
        [Key, ForeignKey("Orders"), Column(Order = 0)] 
        public int orderID { get; set; }
        [Key, ForeignKey("Products"), Column(Order = 1)]
        public int productID { get; set; }
        public int quantity { get; set; }
        public virtual Products Products { get; set; }
        public virtual Orders Orders { get; set; }
    }
}