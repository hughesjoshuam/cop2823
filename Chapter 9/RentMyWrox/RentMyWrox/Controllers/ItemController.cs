﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentMyWrox.Models;

namespace RentMyWrox.Controllers
{
    public class ItemController : Controller
    {

        [Route("")]
        // GET: Item
        public ActionResult Index()
        {
            using (RentMyWroxContext context = new Models.RentMyWroxContext())
            {
                List<Item> itemList = context.Items.Where(x => x.IsAvailable).Take(5).ToList();
                return View(itemList);
            }
        }
        public ActionResult Details(int id)
        {
            using (RentMyWroxContext context = new Models.RentMyWroxContext())
            {
                Item item = context.Items.FirstOrDefault(x => x.id == id);
                return View(item);
            }
        }
    }
}