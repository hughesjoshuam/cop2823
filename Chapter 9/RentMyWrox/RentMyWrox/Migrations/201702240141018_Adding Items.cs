namespace RentMyWrox.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingItems : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Hobbies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.UserDemographics",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Birthdate = c.DateTime(nullable: false),
                        Gender = c.String(),
                        MaritalStatus = c.String(),
                        DateMovedIntoArea = c.DateTime(nullable: false),
                        OwnHome = c.Boolean(nullable: false),
                        TotalPeopleInHome = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        ItemNumber = c.String(),
                        Picture = c.String(),
                        Cost = c.Double(nullable: false),
                        CheckedOut = c.DateTime(),
                        DueBack = c.DateTime(),
                        DateAcquired = c.DateTime(nullable: false),
                        IsAvailable = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.UserDemographicsHobbies",
                c => new
                    {
                        UserDemographics_id = c.Int(nullable: false),
                        Hobby_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserDemographics_id, t.Hobby_id })
                .ForeignKey("dbo.UserDemographics", t => t.UserDemographics_id, cascadeDelete: true)
                .ForeignKey("dbo.Hobbies", t => t.Hobby_id, cascadeDelete: true)
                .Index(t => t.UserDemographics_id)
                .Index(t => t.Hobby_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserDemographicsHobbies", "Hobby_id", "dbo.Hobbies");
            DropForeignKey("dbo.UserDemographicsHobbies", "UserDemographics_id", "dbo.UserDemographics");
            DropIndex("dbo.UserDemographicsHobbies", new[] { "Hobby_id" });
            DropIndex("dbo.UserDemographicsHobbies", new[] { "UserDemographics_id" });
            DropTable("dbo.UserDemographicsHobbies");
            DropTable("dbo.Items");
            DropTable("dbo.UserDemographics");
            DropTable("dbo.Hobbies");
        }
    }
}
